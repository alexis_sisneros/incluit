const { getApprovedUsingFor } = require('../clase-1/clase1')

const students = [
  {
    name: 'JOHN',
    lastname: 'DOE',
    score: 4
  },
  {
    name: 'EVELYN',
    lastname: 'JACKSON',
    score: 8
  },
  {
    name: 'JAMES',
    lastname: 'SHAW',
    score: 2
  }
];

const simulatePromise = (nombre) => {
  const myPromise = new Promise((resolve, reject) => {
    setTimeout(function () {
      if (nombre === "Verde") {
        resolve("Función asincrona RESUELTA a los dos segundos!");
      } else if (nombre === "Rojo") {
        reject("Función asincrona RECHAZADA a los dos segundos");
      }
    }, 2000);
  });

  function success(mensaje) {
    console.log(mensaje)
  }

  function error(mensaje) {
    console.log(mensaje)
  }

  myPromise.then(success).catch(error)
}

const callGetStudents = () => {
  getApprovedUsingFor(students, 4)
}

simulatePromise("Verde");
callGetStudents();
