const { getApprovedUsingFor, getApprovedUsingWhile, getApprovedUsingDoWhile } = require('./clase-1/clase1')

const students = [
  {
    name: 'JOHN',
    lastname: 'DOE',
    score: 4
  },
  {
    name: 'EVELYN',
    lastname: 'JACKSON',
    score: 8
  },
  {
    name: 'JAMES',
    lastname: 'SHAW',
    score: 2
  }
];

console.log("USING FOR: ")
getApprovedUsingFor(students, 4)

console.log("USING WHILE: ")
getApprovedUsingWhile(students, 4)

console.log("USING DO WHILE: ")
getApprovedUsingDoWhile(students, 4)