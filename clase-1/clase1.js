const { capitalizeString, sortStudents } = require("./utils");

const getApprovedUsingFor = (students, minimumNote) => {
  let result = [];
  for (student of students) {
    student.score >= minimumNote && result.push(student);
  }

  result = sortStudents(result);

  result = result.map((student) => {
    result = { ...student };
    result.name = capitalizeString(result.name);
    result.lastname = capitalizeString(result.lastname);
    return result;
  });

  console.log(result);
};

const getApprovedUsingWhile = (students, minimumNote) => {
  let result = [];
  let index = 0;
  while (index < students.length) {
    const student = students[index];
    student.score >= minimumNote && result.push(student);
    index++;
  }

  result = sortStudents(result);

  result = result.map((student) => {
    result = { ...student };
    result.name = capitalizeString(result.name);
    result.lastname = capitalizeString(result.lastname);
    return result;
  });

  console.log(result);
};

const getApprovedUsingDoWhile = (students, minimumNote) => {
  let result = [];
  let index = 0;
  do {
    const student = students[index];
    student.score >= minimumNote && result.push(student);
    index++;
  } while (index < students.length)

  result = sortStudents(result);

  result = result.map((student) => {
    result = { ...student };
    result.name = capitalizeString(result.name);
    result.lastname = capitalizeString(result.lastname);
    return result;
  });

  console.log(result);
};

exports.getApprovedUsingFor = getApprovedUsingFor;
exports.getApprovedUsingWhile = getApprovedUsingWhile;
exports.getApprovedUsingDoWhile = getApprovedUsingDoWhile;
