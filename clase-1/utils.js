const sortStudents = (students) => {
  return students.sort((a, b) => a.score - b.score);
};

const capitalizeString = (str) => {
  return str[0].toUpperCase() + str.slice(1).toLowerCase();
};

exports.sortStudents = sortStudents
exports.capitalizeString = capitalizeString
